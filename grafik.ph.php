<?php 
include "koneksi.php";
 ?>
<script src="js.js"></script>
<canvas id="myChart" width="100%" height="25px"></canvas>
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.5.1/dist/chart.min.js"></script>
<script>
	var ctx = document.getElementById('myChart').getContext('2d');
	var myChart = new Chart(ctx, {
	    type: 'line',
	    data: {
	        labels: [
	        	<?php 
	        	while ($phs1 = mysqli_fetch_assoc($cari_timestamp_ph)) {
	        		echo "'" . $phs1['timestamp'] . "',";
	        	}
	        	 ?>
	        ],
	        datasets: [{
	            label: 'Rekaman PH',
	            data: [
	            <?php 
	        	while ($phs2 = mysqli_fetch_assoc($cari_rekaman_ph)) {
	        		echo (isset($phs2['ph']) ? $phs2['ph'] : 0) . ",";
	        	}
	        	 ?>
	            ],
	            backgroundColor: [
	                'rgba(255, 99, 132, 0.2)'
	            ],
	            borderColor: [
	                'rgba(255, 99, 132, 1)'
	            ],
	            borderWidth: 1
	        }]
	    },
	    options: {
	        scales: {
	            y: {
	                beginAtZero: true
	            }
	        },
	        animation: {
		        duration: 0
		    }
	    }
	});
</script>