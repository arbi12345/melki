<?php 
include 'koneksi.php';
?>

<!DOCTYPE HTML>
<html>
<head>
<script src="js.js"></script>
<script>
window.onload = function() {

	$(function(){
		setInterval(updateData, 1000);
	});

	function addData(data) {
		$('#phtext').text(data.ph);
		$('#kepekatantext').text(data.kep);
		$('#humitext').text(data.humi);
		$('#temptext').text(data.temp);
		$('#timestaptext').text(data.timestamp);

		if (+<?php echo $ba_ph; ?> < data.ph) {
			$('#phtext').css("color", "red");
		} else if (+<?php echo $bb_ph; ?> > data.ph) {
			$('#phtext').css("color", "yellow");
		} else {
			$('#phtext').css("color", "blue");
		}

		if (+<?php echo $ba_pp; ?> < data.kep) {
			$('#kepekatantext').css("color", "red");
		} else if (+<?php echo $bb_pp; ?> > data.kep) {
			$('#kepekatantext').css("color", "yellow");
		} else {
			$('#kepekatantext').css("color", "blue");
		}
	}

	function updateData() {
		$.getJSON("data.php", addData);
		document.getElementById("phIframe").src="grafik.ph.php";
		document.getElementById("kepIframe").src="grafik.kep.php";
		document.getElementById("suhuIframe").src="grafik.suhu.php";
		document.getElementById("humiIframe").src="grafik.humi.php";
	}

}
</script>
</head>
<body>
	<div style="width: 100%; display:inline-block;">
		<table width="100%">
			<tr>
				<td>	
					<div id="PHchart" style="height: auto; width: 100%;align-items: center; display:inline-block;">
						<h1 style="text-align: center;width: 100%;"> PH </h1>
						<h1 style="size: 100px;text-align: center;width: 100%;" id="phtext"></h1>
					</div>
				</td>
				<td>	
					<div id="KEPEKATANchart" style="height: auto; width: 100%;align-items: center; display:inline-block;">
						<h1 style="text-align: center;width: 100%;"> Kepekatan </h1>
						<h1 style="size: 100px;text-align: center;width: 100%;" id="kepekatantext"></h1>
					</div>	
				</td>
			</tr>
			<tr>
				<td>
					<div id="KEPEKATANachart" style="height: auto; width: 100%;align-items: center; display:inline-block;">
						<h1 style="text-align: center;width: 100%;"> Temperatur </h1>
						<h1 style="size: 100px;text-align: center;width: 100%;" id="temptext"></h1>
					</div>	
				</td>
				<td>
					<div id="KEPEKATANschart" style="height: auto; width: 100%;align-items: center; display:inline-block;">
						<h1 style="text-align: center;width: 100%;"> Kelembaban </h1>
						<h1 style="size: 100px;text-align: center;width: 100%;" id="humitext"></h1>
					</div>	
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div id="s" style="height: auto; width: 100%;align-items: center; display:inline-block;">
						<h1 style="text-align: center;width: 100%;"> Update Terakhir </h1>
						<h1 style="size: 100px;text-align: center;width: 100%;" id="timestaptext"></h1>
					</div>	
				</td>
			</tr>
		</table>
	</div>
	<div style="width: 100%;">
		<iframe id="phIframe" style="border-width:0" width=100% height='500px' frameborder="0" scrolling="no"></iframe>
	</div>
	<div style="width: 100%;">
		<iframe id="kepIframe" style="border-width:0" width=100% height='500px' frameborder="0" scrolling="no"></iframe>
	</div>
	<div style="width: 100%;">
		<iframe id="suhuIframe" style="border-width:0" width=100% height='500px' frameborder="0" scrolling="no"></iframe>
	</div>
	<div style="width: 100%;">
		<iframe id="humiIframe" style="border-width:0" width=100% height='500px' frameborder="0" scrolling="no"></iframe>
	</div>
	<div Style="margin:auto; width:65%; ">
		<div class="menu" id="PHchart" style="height: auto;align-items: center; display:inline-block;">
			<ol>
			<li>
			<a href="index.php">
				<h3 style="text-align: center;">
				<img src="img/house.png" width="50px"><br>
				 Beranda </h3>
			</a>
			</li>
			<li>
			<a href="manual.php">
				<h3 style="text-align: center;"> 
				<img src="img/tasks.png" width="50px"><br>
				Input Manual </h3>
			</a>
			</li>
			<li>
			<a href="config.php">
				<h3 style="text-align: center;">
				<img src="img/settings.png" width="50px"><br>
				 Konfigurasi </h3>
			</a>
			</li>
			</ol>
		</div>
	</div>
</body>
</html>
