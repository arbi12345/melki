<?php
$con = mysqli_connect("localhost","root","","melki");

if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  exit();
}

$sql = "SELECT `ph`, `kep`, `temp`, `humi`, `timestamp` FROM `data` ORDER BY id DESC limit 1;";

if ($result = mysqli_query($con, $sql)) {
  // Fetch one and one row
  $row = mysqli_fetch_assoc($result);
  $ph  = $row['ph'];
  $kep = $row['kep'];
  $temp = $row['temp'];
  $humi = $row['humi'];
  $timestamp = $row['timestamp'];

  $data = [
  		'ph' => $ph,
  		'kep' => $kep,
      'temp' => $temp,
      'humi' => $humi,
  		'timestamp' => $timestamp
  ];
  $data = json_encode($data);

}

$sql2 = "SELECT * FROM `config` where id = 1 limit 1;";

if ($result2 = mysqli_query($con, $sql2)) {
  // Fetch one and one row
  $row2 = mysqli_fetch_assoc($result2);
  $ba_ph  = $row2['ba_ph'];
  $ba_pp = $row2['ba_pp'];
  $bb_ph  = $row2['bb_ph'];
  $bb_pp = $row2['bb_pp'];

}

$cari_rekaman_ph   = mysqli_query($con, "SELECT * FROM (SELECT id, ph, `timestamp` FROM records WHERE ph IS NOT NULL ORDER BY id DESC Limit 15)var1 ORDER BY id ASC;");
$cari_timestamp_ph   = mysqli_query($con, "SELECT * FROM (SELECT id, ph, `timestamp` FROM records WHERE ph IS NOT NULL ORDER BY id DESC Limit 15)var1 ORDER BY id ASC;");
$jml_ph            = mysqli_num_rows($cari_rekaman_ph);

$cari_rekaman_kep  = mysqli_query($con, "SELECT * FROM (SELECT id, kep, `timestamp` FROM records WHERE kep IS NOT NULL ORDER BY id DESC Limit 15)var1 ORDER BY id ASC;");
$cari_timestamp_kep  = mysqli_query($con, "SELECT * FROM (SELECT id, kep, `timestamp` FROM records WHERE kep IS NOT NULL ORDER BY id DESC Limit 15)var1 ORDER BY id ASC;");
$jml_kep           = mysqli_num_rows($cari_rekaman_kep);

$cari_rekaman_temp = mysqli_query($con, "SELECT * FROM (SELECT id, temp, `timestamp` FROM records WHERE temp IS NOT NULL ORDER BY id DESC Limit 15)var1 ORDER BY id ASC;");
$cari_timestamp_temp = mysqli_query($con, "SELECT * FROM (SELECT id, temp, `timestamp` FROM records WHERE temp IS NOT NULL ORDER BY id DESC Limit 15)var1 ORDER BY id ASC;");
$jml_temp          = mysqli_num_rows($cari_rekaman_temp);

$cari_rekaman_humi = mysqli_query($con, "SELECT * FROM (SELECT id, humi, `timestamp` FROM records WHERE humi IS NOT NULL ORDER BY id DESC Limit 15)var1 ORDER BY id ASC;");
$cari_timestamp_humi = mysqli_query($con, "SELECT * FROM (SELECT id, humi, `timestamp` FROM records WHERE humi IS NOT NULL  ORDER BY id DESC Limit 15)var1 ORDER BY id ASC;");
$jml_humi          = mysqli_num_rows($cari_rekaman_humi);
?>